# Renovate

> Renovate is a bot that creates pull requests to update your dependencies and lock files on various platforms.

## Usage

> :warning: **Attention**: Renovate est un bot qui va créer des _merge requests_ il faut donc que les merge requests soient activées sur votre projet.

Pour ajouter Renovate a votre projet, il vous suffit d'ajouter le bot [@renovate](https://git.inpt.fr/renovate) comme _developer_ ou bien _maintainer_ de votre projet. 

Lors de la prochaine execution de Renovate (toutes les heures à la minute 42), il va créer un fichier de configuration `.renovaterc.json` qui contient les configurations par défaut et ouvrir une _merge request_ avec celui-ci. Vous pouvez modifier ce fichier pour ajouter des configurations spécifiques à votre projet.

Enfin, pour activer Renovate, il suffit de _merge_ la _merge request_.

## Configuration

Pour configurer Renovate, merci de consulter la [documentation officielle](https://docs.renovatebot.com/).